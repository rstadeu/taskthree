![alt text](https://img.shields.io/npm/v/react-icons.svg?style=flat-square) ![alt text](https://travis-ci.com/react-icons/react-icons.svg?branch=master)
# Task3 (Components lecture)

Write components implementing HTML markup for required design (see images at the beginning of the document). For this part, no need to implement API calls and routing, the task can be done with mocked data.

Use ErrorBoundary component for catching and displaying errors ( https://reactjs.org/docs/error-boundaries.html ). You could create one component and wrap all your application, or use several components.

Use smart/dumb components approach

100% decomposition (estimated by mentor)

Evaluation criteria:

(each mark includes previous mark criteria)

|                   2                  |                       3                       |                          4                          |                                  5                                  |
|:------------------------------------:|:---------------------------------------------:|:---------------------------------------------------:|:-------------------------------------------------------------------:|
| Markup is done with React Components | Use ErrorBoundary component for catching and displaying errors | Use smart/dumb components approach | 100% decomposition ( evaluated by mentor )|

## Running the project

Install all dependencies
```
npm install
```

To Run 
```
npm run start
```
