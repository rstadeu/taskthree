export const search = [
  {name: 'title', prop: 'show_title'},
  {name: 'director', prop: 'director'}
]

export const sort = [
  {name: 'release date', prop: 'release_year'},
  {name: 'rating', prop: 'rating'}
]

