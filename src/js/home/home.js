import React from 'react'
import commonStyles from '../../style/common'

const Home = () => (
  <div style={commonStyles.formGroup}>
    <div style={commonStyles.panelDescription}>No films found</div>
  </div>
)

export default Home
