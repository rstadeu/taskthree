import React from 'react'
import { Col } from 'react-bootstrap'
import commonStyles from '../../style/common'

class FilmSubHeader extends React.Component {
  constructor(props) {
    super(props)

    this.director = 'Quentin Tarantino'
  }
  render() {
    return (
      <Col xs={12} sm={12} md={12} style={commonStyles.block}>
        <span style={commonStyles.text_block}>Films by {this.director} </span>
      </Col>
    )
  }
}

export default FilmSubHeader
